package com.zuitt.example;

import java.util.Scanner;

public class S2A1 {
    public static void main(String[] args){

        System.out.println("Input year to be checked if a leap year.");
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();

        if (((year % 4 == 0) && (year % 100!= 0)) || (year%400 == 0)) {
            System.out.println(year + " is a leap year");
        }
        else {
            System.out.println(year + " is NOT a leap year");
        }
    }
}
