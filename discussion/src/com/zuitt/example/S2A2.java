package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class S2A2 {
    public static void  main(String[] args){
        int[] intArray = new int[5];

        intArray[0] = 2;
        intArray[1] = 3;
        intArray[2] = 5;
        intArray[3] = 7;
        intArray[4] = 11;

        System.out.println("The first prime number is: " + intArray[0]);

        String[] names = {};

        ArrayList<String> friends = new ArrayList<String>(Arrays.asList(names));

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoe");

        System.out.println(friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventory);



    }
}
